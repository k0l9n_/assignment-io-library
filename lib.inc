section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
   xor rax, rax
   .loop:
   	cmp byte [rdi + rax], 0
   	je .end
   	inc rax
   	jmp .loop
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi,10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi,10
    mov rsi, rsp
    dec rsp
    mov [rsp], byte 0
    .loop:
         xor rdx,rdx
	 div rdi
	 add rdx, '0'
         dec rsp
         mov [rsp], dl
         test rax,rax
         jnz .loop
    .end:
         mov rdi,rsp
         push rsi
         call print_string
         pop rsp
         ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns print_uint
    push rdi
    mov rdi,'-'
    call print_char
    pop rdi
    neg rdi
    jns print_uint

    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе (rdi,rsi)
string_equals:
    call string_length
    mov rcx, rax ;rcx = len1
    ;change rdi and rsi
    xchg rdi, rsi
    ;end of changing
    call string_length ;rax = len2
    cmp rax,rcx
    jnz .error
    xor rdx,rdx   ;counter
    .loop:
	xor rax,rax
	mov al,byte[rsi+rdx]
	cmp byte[rdi+rdx], al
    	jnz .error
	cmp rdx,rcx
	jz .ok
        inc rdx
	jmp .loop
    .error:
	xor rax,rax
	ret
    .ok: 
	xor rax,rax
	mov rax,1
	ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov [rsp], al
    mov rsi, rsp
    mov rdx,1
    syscall
    mov al, [rsp]
    inc rsp
    ret 

; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push r12
    push r13
    push r14
    mov r12, rdi ; buf start
    mov r13,rsi ; buf size
    xor r14,r14
    .check:
        test r14,r14
        jnz .end
    .loop:
         cmp r14,r13
         jae .overflow
         call read_char
         cmp al,0x20
         je .check
         cmp al,0x9
         je .check
         cmp al,0xA
         je .check
	 test al,al
	 jz .end
         mov [r12+r14],al
         inc r14
         jmp .loop
   .overflow:
         xor rax,rax
         jmp .back
   .end:
	 mov [r12+r14], byte 0
         mov rax, r12
	 mov rdx, r14
    .back:
	 pop r12
         pop r13
         pop r14
         ret
 

; Принимает указатель на строку(rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length
    mov rcx,rax
    xor rax, rax ; number
    xor rdx,rdx ; lenght
    mov rsi,rdi
    
    .loop:
	xor rdi,rdi
	mov dil, byte[rsi+rdx]
        cmp rdi,'0'
        jb .end
	cmp rdi,'9'
	ja .end
	sub rdi,'0'
	imul rax,10
	add rax, rdi
	inc rdx
	dec rcx
	jnz .loop 
    .end:
	ret




; Принимает указатель на строку(rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi],'-'
    jne parse_uint
    .minus:
          inc rdi
          call parse_uint
          test rax,rax
	  jz .null
	  neg rax
          inc rdx
          ret
    .null:
	  ret



; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9
    call string_length
    inc rax
    cmp rax, rdx	     
    jnz .fall
    .loop:
        mov cl,byte[rdi+r9]
	mov byte[rsi+r9],cl
        inc r9
        cmp r9,rax
        jnz .loop
    .end:
        dec rax
    	ret
    .fall:
	mov rax, 0
        ret

